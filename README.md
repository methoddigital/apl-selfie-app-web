<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Gatsby minimal starter
</h1>

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the minimal starter.

    ```shell
    # create a new Gatsby site using the minimal starter
    npm init gatsby
    ```

2.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```shell
    cd my-gatsby-site/
    npm run develop
    ```

3.  **Open the code and start customizing!**

    Your site is now running at http://localhost:8000!

    Edit `src/pages/index.js` to see your site update in real-time!

4.  **Learn more**

    - [Documentation](https://www.gatsbyjs.com/docs/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Tutorials](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Guides](https://www.gatsbyjs.com/tutorial/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [API Reference](https://www.gatsbyjs.com/docs/api-reference/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Plugin Library](https://www.gatsbyjs.com/plugins?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

    - [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter)

## 🚀 Quick start (Gatsby Cloud)

Deploy this starter with one click on [Gatsby Cloud](https://www.gatsbyjs.com/cloud/):

[<img src="https://www.gatsbyjs.com/deploynow.svg" alt="Deploy to Gatsby Cloud">](https://www.gatsbyjs.com/dashboard/deploynow?url=https://github.com/gatsbyjs/gatsby-starter-minimal)



Project
# Node and Dependencies
nvm use 16.13.1
node -v
npm install

# Run
npm run develop 
or
gatsby develop
http://localhost:8000/

# Test over network
gatsby develop -H 192.168.0.138
gatsby develop -H 172.20.10.2

http://192.168.0.138:8000/
http://172.20.10.2:8000/

# Build
gatsby build

# Local development Cache issues
gatsby clean

# Init Firebase
firebase logout
firebase login
firebase init

# Deploy
gatsby build
firebase deploy
https://apl-selfie-app.web.app/


### firebase commands
# firebase add alias environments
firebase use --add  
firebase use development 
firebase use staging 
firebase use production  
# firebase target hosting
firebase target:apply hosting development apl-selfie-app-staging
firebase target:apply hosting staging apl-selfie-app-staging
firebase target:apply hosting production apl-selfie-app-prod
# firebase deploy hosting to target

# firebase STAGING Deploy
STAGING=true gatsby develop
STAGING=true gatsby build
firebase deploy --only hosting:staging
# firebase functions
firebase use staging 
firebase deploy --only functions   


# firebase PRODUCTION Deploy
PRODUCTION=true gatsby develop
PRODUCTION=true gatsby build
firebase deploy --only hosting:production
# firebase functions
firebase use production 
firebase deploy --only functions   

#reCAPTCHA
sitekey
6Ld-gIoeAAAAAOPqPfaugF-dpUy1J-jgYGL8NyRj
secretkey
6Ld-gIoeAAAAACPHdovpt_P5Z-q1J8hcNUwmE9Wm



